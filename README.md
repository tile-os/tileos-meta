#### Main metapackages for TileOS

Contains:

* tileos-base - core components of TileOS
* tileos-desktop-common - common desktop-specific componments
* tileos-desktop-sway - Sway wayland compositor and it's dependencies
* tileos-desktop-swayfx - SwayFX wayland compositor and it's dependencies (highly experimental, may break your system!)
* tileos-desktop-qtile - Qtile window manager and it's dependencies (highly experimental, may break your system!)
* tileos-desktop-river - River wayland compositor and it's dependencies (experimental)
* tileos-drivers - metapackage for installing additional drivers
* tileos-sway-live - LiveCD components for TileOS Sway edition
* tileos-swayfx-live - LiveCD components for TileOS SwayFX edition
* tileos-qtile-live - LiveCD components for TileOS Qtile edition
* tileos-river-live - LiveCD components for TileOS River edition
